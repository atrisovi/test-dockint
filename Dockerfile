FROM rootproject/root-ubuntu16
USER root

# Create a user that does not have root privileges 
ARG username=anatris
RUN useradd --create-home --home-dir /home/${username} ${username}
ENV HOME /home/${username}
USER ${username}
WORKDIR /home/${username}

# Copy the hello.c file to our current working directory
COPY hello.C .

# When starting the container and no command is started, run hello.c in ROOT and quit.
CMD ["root.exe", "-q", "hello.C"]

